#!/bin/bash
set -e

buildid=$1
dpath=blog-tale-java
[ -e $dpath ] && rm -r $dpath
[ -f "blog-tale-java.deb" ] && rm -f blog-tale-java.deb

mkdir -p $dpath $dpath/DEBIAN $dpath/usr/bin $dpath/opt/blog-tale/ $dpath/etc/systemd/system
echo -e "Package: blog-tale-java\nVersion: $buildid\nMaintainer: MedvedevYuriy\nSection: misc\nDescription: web blog tale on java\nArchitecture: all"  > $dpath/DEBIAN/control
echo -e "#!/bin/bash\nsystemctl daemon-reload\nsystemctl start blog-tale-java\nsystemctl enable blog-tale-java"  > $dpath/DEBIAN/postinst

chmod 0755 $dpath/DEBIAN/postinst

cp tale-latest.jar $dpath/opt/blog-tale/
cp tool $dpath/opt/blog-tale/
cp -r lib $dpath/opt/blog-tale/
cp -r resources $dpath/opt/blog-tale/
cp tale.zip $dpath/opt/blog-tale/

FILE=$dpath/etc/systemd/system/blog-tale-java.service
cat << EOF > $FILE
[Unit]
Description=Web Blog Tale
After=network.target
[Service]
Type=simple
ExecStart=/usr/bin/java -jar /opt/blog-tale/tale-latest.jar
User=root
[Install]
WantedBy=multi-user.target
EOF

md5deep -r $dpath > $dpath/DEBIAN/md5sums

fakeroot dpkg-deb --build $dpath

exit 0
